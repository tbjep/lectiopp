#include <catch2/catch.hpp>
#include <lectiopp/lectiopp.hpp>

TEST_CASE("List of schools is not empty", "[school_list]") {
	REQUIRE(!lectiopp::available_schools().empty());
}

std::string school_name_from_id(std::uint16_t id) {
	return lectiopp::school(id).name();
}

TEST_CASE("First 10 school names match", "[school_names]") {
	auto schools = lectiopp::available_schools();
	auto school_name_matches = [schools](decltype(schools)::size_type index) {
		const auto list_scraped_name = schools[index].name();
		const auto main_page_scraped_name = school_name_from_id(schools[index].id());
		return list_scraped_name == main_page_scraped_name;
	};
	REQUIRE(school_name_matches(0));
	REQUIRE(school_name_matches(1));
	REQUIRE(school_name_matches(2));
	REQUIRE(school_name_matches(3));
	REQUIRE(school_name_matches(4));
	REQUIRE(school_name_matches(5));
	REQUIRE(school_name_matches(6));
	REQUIRE(school_name_matches(7));
	REQUIRE(school_name_matches(8));
	REQUIRE(school_name_matches(9));
}
