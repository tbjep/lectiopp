#include <iostream>
#include <lectiopp/lectiopp.hpp>

int main() {
	const auto schools = lectiopp::available_schools();
	std::cout << "School count: " << schools.size()
	          << "\nSchools:\n";
	for (const auto &school : schools) {
		std::cout << school.name() << '\n';
	}
	return 0;
}
