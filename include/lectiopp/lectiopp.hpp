#ifndef LECTIOPP_LECTIOPP_HPP
#define LECTIOPP_LECTIOPP_HPP

#include <cpr/cpr.h>
#include <iostream>
#include <pugixml.hpp>
#include <string_view>
#include <vector>
#include <functional>
#include <locale>

namespace lectiopp {

namespace detail {

// More status codes should be added as they are needed
enum class http_status_code : std::int32_t {
	ok = 200,
};

constexpr std::string_view base_url{"https://www.lectio.dk/lectio/"};

constexpr std::string_view school_list_url{"https://www.lectio.dk/lectio/login_list.aspx?showall=1"};

constexpr std::string_view main_menu_path{"default.aspx"};

auto is_whitespace(char c) {
	return std::isspace(c, std::locale::classic());
}

} // namespace detail

class school {
	std::uint16_t id_;
	std::string name_;

	[[nodiscard]] school(std::uint16_t id, std::string name) : id_(id), name_(std::move(name)) {}

	template <template <typename> typename Return>
    friend Return<school> available_schools();

public:
	[[nodiscard]] explicit school(std::uint16_t id) : id_(id) {
		// Get name of school
		auto response = cpr::Get(cpr::Url{std::string(base_url()) + std::string(detail::main_menu_path)});
		using detail::http_status_code;
		if (http_status_code{response.status_code} == http_status_code::ok && response.header["content-type"] == "text/html; charset=utf-8") {
			pugi::xml_document document;
			auto parse_result = document.load_buffer(response.text.data(), response.text.length(), pugi::parse_full);
			if (parse_result) {
				auto name_node = document.document_element().select_node(R"(//*[@id="m_masterleftDiv"])");
				const auto name_source = std::string_view(name_node.node().text().get());
				const auto first_character = std::find_if_not(name_source.begin(), name_source.end(), detail::is_whitespace);
				const auto last_character = std::find(first_character, name_source.end(), '\n');
				name_ = {first_character, last_character};
			} else {
				throw std::logic_error("Could not parse document");
			}
		} else {
			throw std::logic_error("Could not get page for school");
		}
	}

	[[nodiscard]] std::uint16_t id() const {
		return id_;
	}

	[[nodiscard]] std::string base_url() const {
		return std::string(detail::base_url) + std::to_string(id()) + '/';
	}

	[[nodiscard]] std::string name() const {
		return name_;
	}
};

template <template <typename> typename Return = std::vector>
Return<school> available_schools() {
	auto response = cpr::Get(cpr::Url{detail::school_list_url});
	using detail::http_status_code;
	if (http_status_code{response.status_code} == http_status_code::ok && response.header["content-type"] == "text/html; charset=utf-8") {
		pugi::xml_document document;
		pugi::xml_parse_result parse_result = document.load_buffer(response.text.data(), response.text.length(), pugi::parse_full);
		if (parse_result) {
			auto document_node = document.document_element();
			auto schools_node = document_node.child("body").child("div");
			Return<school> out;
			for (const auto &child : schools_node.children()) {
				auto anchor = child.first_child();
				std::string href_text = anchor.attribute("href").value();

				constexpr auto start_pos = sizeof("/lectio/") - 1;
				auto count = href_text.size() - start_pos - (sizeof("/default.aspx") - 1);
				auto school_id = href_text.substr(start_pos, count);

				out.push_back(school{static_cast<std::uint16_t>(std::stoi(school_id)), anchor.text().get()});
			}
			return out;
		} else {
			throw std::logic_error("Could not parse document");
		}
	} else {
		throw std::logic_error("Invalid data from lectio");
	}
}

} // namespace lectiopp

#endif
